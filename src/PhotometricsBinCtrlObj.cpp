//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2020
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043
// FRANCE
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#include "PhotometricsBinCtrlObj.h"
#include "PhotometricsException.h"

using namespace lima;
using namespace lima::Photometrics;

BinCtrlObj::BinCtrlObj(short cam) : m_cam(cam)
{
  DEB_CONSTRUCTOR();
  
  rs_bool available_ser,available_par;
  CHECK_PVCAM(pl_get_param(m_cam, PARAM_BINNING_SER, ATTR_AVAIL, (void *)&available_ser));
  CHECK_PVCAM(pl_get_param(m_cam, PARAM_BINNING_PAR, ATTR_AVAIL, (void *)&available_par));
  if(available_par && available_ser)
    {
      uns32 count;
      CHECK_PVCAM(pl_get_param(m_cam,PARAM_BINNING_PAR,ATTR_COUNT,&count));
      for(int i = 0;i < count;++i)
	{
	  int32 x_bin;
	  CHECK_PVCAM(pl_get_enum_param(m_cam, PARAM_BINNING_PAR, i, &x_bin, NULL, 0));
	  int32 y_bin;
	  CHECK_PVCAM(pl_get_enum_param(m_cam, PARAM_BINNING_SER, i, &y_bin, NULL, 0));
	  m_possible_bin.push_back({x_bin,y_bin});
	}
    }
}

void BinCtrlObj::setBin(const Bin& bin)
{
  m_bin = bin;
}
void BinCtrlObj::getBin(Bin& bin)
{
  bin = m_bin;
}
void BinCtrlObj::checkBin(Bin& bin)
{
  if(m_possible_bin.empty())	// do not support binning
    bin = Bin(1,1);		// force no binning
  else
    {
      /* Not very clever; 
	 validate the binning only if x_bin and y_bin
	 match one of available.
	 otherwise don't bin
      */
      for(auto available_bin=m_possible_bin.begin();
	  available_bin != m_possible_bin.end();++available_bin)
	if(*available_bin == bin)
	  return;		// Validate the binning
      bin = Bin(1,1);		// otherwise don't bin at all ;)
    }
}
