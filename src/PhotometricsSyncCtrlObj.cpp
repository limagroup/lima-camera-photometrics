//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2020
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043
// FRANCE
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#include "PhotometricsSyncCtrlObj.h"
#include "PhotometricsException.h"

using namespace lima;
using namespace lima::Photometrics;


SyncCtrlObj::SyncCtrlObj(short cam) :
  m_cam(cam),m_trig_mode(IntTrig)
{
  
}

bool SyncCtrlObj::checkTrigMode(TrigMode mode)
{
  DEB_MEMBER_FUNCT();
  bool valid_mode;
  switch(mode)
    {
    case IntTrig:
    case IntTrigMult:
    case ExtTrigSingle:
    case ExtTrigMult:
    case ExtGate:
      valid_mode = true;
      break;
    default:
      valid_mode = false;
      break;
    }
  return valid_mode;
}


void SyncCtrlObj::setTrigMode(TrigMode trig_mode)
{
  m_trig_mode = trig_mode;
}

void SyncCtrlObj::getTrigMode(TrigMode& trig_mode)
{
  trig_mode = m_trig_mode;
}

int SyncCtrlObj::getHwTrigMode() const
{
  DEB_MEMBER_FUNCT();
  
  int trigger_mode;
  switch(m_trig_mode)
    {
    case IntTrig:
    case IntTrigMult:
      trigger_mode = TIMED_MODE;
      break;
    case ExtTrigSingle:
      trigger_mode = TRIGGER_FIRST_MODE;
      break;
    case ExtTrigMult:
      trigger_mode = STROBED_MODE;
      break;
    case ExtGate:
      trigger_mode = BULB_MODE;
      break;
    default:
      THROW_HW_ERROR(Error) << "Trigger not managed" << DEB_VAR1(m_trig_mode);
    }
  return trigger_mode;
}

void SyncCtrlObj::setExpTime(double exp_time)
{
  m_expo_time = exp_time;
}

void SyncCtrlObj::getExpTime(double &exp_time)
{
  exp_time = m_expo_time;
}

int32 SyncCtrlObj::getHwExpTime() const
{
  int32 expo_res = _get_expo_res();
  return int32(m_expo_time * expo_res);
}

void SyncCtrlObj::setLatTime(double lat_time)
{
  //Not managed
}

void SyncCtrlObj::getLatTime(double& lat_time)
{
  lat_time = 0;
}

void SyncCtrlObj::setNbHwFrames(int nb_frames)
{
  m_acq_nb_frames = nb_frames;
}

void SyncCtrlObj::getNbHwFrames(int& nb_frames)
{
  nb_frames = m_acq_nb_frames;
}
void SyncCtrlObj::getValidRanges(ValidRangesType& valid_ranges)
{
  DEB_MEMBER_FUNCT();
  
  ulong64 min_expo,max_expo;
  CHECK_PVCAM(pl_get_param(m_cam, PARAM_EXPOSURE_TIME, ATTR_MIN, &min_expo));
  CHECK_PVCAM(pl_get_param(m_cam, PARAM_EXPOSURE_TIME, ATTR_MAX, &max_expo));

  int32 expo_res = _get_expo_res();
  valid_ranges.min_exp_time = min_expo / expo_res;
  valid_ranges.max_exp_time = max_expo / expo_res;
  valid_ranges.min_lat_time = 0.;
  valid_ranges.max_lat_time = 0.;
}

int32 SyncCtrlObj::_get_expo_res() const
{
  DEB_MEMBER_FUNCT();
  
  rs_bool isAvailable = false;
  //CHECK_PVCAM(pl_get_param(m_cam,PARAM_EXP_RES, ATTR_AVAIL, (void *)&isAvailable));
  int32 expo_res;
  if(!isAvailable)
    expo_res = 1e3;		// millisecond on old camera
  else
    CHECK_PVCAM(pl_get_param(m_cam, PARAM_EXP_RES, ATTR_CURRENT, &expo_res));
  return expo_res;
}
