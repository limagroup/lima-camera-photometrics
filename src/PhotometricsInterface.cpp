//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2020
// European Synchrotron Radiation Facility
// CS40220 38043 Grenoble Cedex 9 
// FRANCE
//
// Contact: lima@esrf.fr
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#include "PhotometricsInterface.h"
#include "PhotometricsDetInfoCtrlObj.h"
#include "PhotometricsSyncCtrlObj.h"
#include "PhotometricsBinCtrlObj.h"
#include "PhotometricsRoiCtrlObj.h"

#include "PhotometricsInterface.h"
#include "PhotometricsException.h"

#include "processlib/SinkTask.h"
#include "processlib/TaskMgr.h"
 
namespace lima {
    namespace Photometrics {

        // Callback 
        void EOFCallbackHandler(FRAME_INFO* pFrameInfo, void* pContext)
        {
            Interface* _interface = (Interface*)pContext;
            _interface->newFrameReady();
        }

        // _StopAcq
        class _StopAcq : public SinkTaskBase
        {
        public:
            _StopAcq(Interface& anInterface) : m_interface(anInterface) {}
            virtual ~_StopAcq() {}
            virtual void process(Data&)
            {
                m_interface.stopAcq();
            }
        private:
            Interface& m_interface;
        };

        Interface::Interface(const std::string& camera_name) :
            m_cam(-1), m_status(Ready),
            m_det_info(NULL),
            m_sync(NULL),
            m_bin(NULL),
            m_roi(NULL),
            m_pixel_stream(NULL),
            m_exp_frame_bytes(-1)
        {
            DEB_CONSTRUCTOR();
            CHECK_PVCAM(pl_pvcam_init());

            int16 nb_cam = 0;
            CHECK_PVCAM(pl_cam_get_total(&nb_cam));
            // Exit if no cameras have been found
            if (nb_cam == 0)
                THROW_HW_ERROR(Error) << "No cameras found in the system\n";

            // Try to find the camera.
            // if camera_name is empty, open the first found.
            std::list<std::string> cam_found;
            char name[CAM_NAME_LEN];
            for (int i = 0; i < nb_cam; ++i)
            {
                CHECK_PVCAM(pl_cam_get_name(i, name));
                if (camera_name.empty() || camera_name == name)
                {
                    CHECK_PVCAM(pl_cam_open(name, &m_cam, OPEN_EXCLUSIVE));
                    m_cam_name = name;
                    break;
                }
                cam_found.push_back(name);
            }
            if (m_cam < 0)			// Camera not found
            {
                std::string cam_available;
                for (auto i = cam_found.begin(); i != cam_found.end(); ++i)
                {
                    if (cam_available.empty())
                        cam_available = *i;
                    else
                        cam_available = cam_available + "," + *i;
                }

                THROW_HW_ERROR(Error) << "Camera named " << camera_name
                    << " wasn't found...\n"
                    << " Camera available are" << cam_available;
            }
            //Install the callback when image is acquired
            pl_cam_register_callback_ex3(m_cam, PL_CALLBACK_EOF,
                (void*)EOFCallbackHandler,
                this);

            // HW Caps
            m_det_info = new DetInfoCtrlObj(m_cam);
            m_sync = new SyncCtrlObj(m_cam);
            m_bin = new BinCtrlObj(m_cam);
            m_roi = new RoiCtrlObj(m_cam, *m_det_info);

            // Double buffer
            Size max_image_size;
            m_det_info->getMaxImageSize(max_image_size);
            ImageType det_image_type;
            m_det_info->getDefImageType(det_image_type);
            FrameDim frame_dim(max_image_size, det_image_type);
#ifdef __unix
            if (posix_memalign(&m_pixel_stream, 16, frame_dim.getMemSize()))
#else  /* window */
            m_pixel_stream = _aligned_malloc(frame_dim.getMemSize(), 16);
            if (!m_pixel_stream)
#endif
                THROW_HW_ERROR(Error) << "Can't allocate double buffer";

            // Cap list
            m_cap_list.push_back(HwCap(m_det_info));
            m_cap_list.push_back(HwCap(m_sync));
            m_cap_list.push_back(HwCap(m_bin));
            m_cap_list.push_back(HwCap(m_roi));
            m_cap_list.push_back(HwCap(&m_buffer_ctrl_obj));
        }

        Interface::~Interface()
        {
            DEB_DESTRUCTOR();

            delete m_det_info;
            delete m_sync;
            delete m_bin;
            delete m_roi;

            if (m_cam > 0)
                pl_cam_close(m_cam);
            pl_pvcam_uninit();

            _freePixelBuffer();
        }


        std::list<Process> Interface::getPostProcessing() const
        {
            DEB_MEMBER_FUNCT();

            std::list<Process> process;
            rs_bool isAvailable;
            CHECK_PVCAM(pl_get_param(m_cam, PARAM_PP_INDEX, ATTR_AVAIL, (void*)&isAvailable));
            if (isAvailable)
            {
                uns32 featCount;
                CHECK_PVCAM(pl_get_param(m_cam, PARAM_PP_INDEX, ATTR_COUNT, &featCount));
                for (int featureIndex = 0; featureIndex < featCount; ++featureIndex)
                {
                    char featureName[MAX_PP_NAME_LEN];
                    uns32 featureID;
                    CHECK_PVCAM(pl_set_param(m_cam, PARAM_PP_INDEX, &featureIndex));
                    CHECK_PVCAM(pl_get_param(m_cam, PARAM_PP_FEAT_NAME, ATTR_CURRENT, featureName));
                    CHECK_PVCAM(pl_get_param(m_cam, PARAM_PP_FEAT_ID, ATTR_CURRENT, &featureID));
                    process.push_back({ featureID,featureName });
                }
            }
            return process;
        }


        void Interface::getCapList(CapList& cap_list) const
        {
            cap_list = m_cap_list;
        }

        void Interface::reset(ResetLevel reset_level)
        {
            DEB_MEMBER_FUNCT();
            DEB_PARAM() << DEB_VAR1(reset_level);
        }

        void Interface::prepareAcq()
        {
            DEB_MEMBER_FUNCT();
            m_acq_frames = -1;

            int32 expo_time = m_sync->getHwExpTime();
            int nb_frames;
            m_sync->getNbHwFrames(nb_frames);
            TrigMode trig_mode;
            m_sync->getTrigMode(trig_mode);
            int exp_mode = m_sync->getHwTrigMode();
            Roi hw_roi;
            m_roi->getRoi(hw_roi);
            Bin hw_bin;
            m_bin->getBin(hw_bin);


            Point top_left = hw_roi.getTopLeft();
            Point bottom_right = hw_roi.getBottomRight();

            rgn_type binroi = { uns16(top_left.x),uns16(bottom_right.x),uns16(hw_bin.getX()),
                       uns16(top_left.y),uns16(bottom_right.y),uns16(hw_bin.getY()) };

            uns32 exp_bytes;
            CHECK_PVCAM(pl_exp_setup_cont(m_cam, 1, &binroi,
                exp_mode, expo_time, &exp_bytes, CIRC_OVERWRITE));
            if (exp_bytes != m_exp_frame_bytes)
            {
                _freePixelBuffer();
#ifdef __unix
                if (posix_memalign(&m_pixel_stream, 16, exp_bytes * 2))
#else  /* window */
                m_pixel_stream = _aligned_malloc(exp_bytes * 2, 16);
                if (!m_pixel_stream)
#endif
                    THROW_HW_ERROR(Error) << "Can't allocate double buffer";

                m_exp_frame_bytes = exp_bytes;
            }
        }


        void Interface::startAcq()
        {
            DEB_MEMBER_FUNCT();
            CHECK_PVCAM(pl_exp_start_cont(m_cam, m_pixel_stream, m_exp_frame_bytes * 2))
        }


        void Interface::stopAcq()
        {
            DEB_MEMBER_FUNCT();
            CHECK_PVCAM(pl_exp_stop_cont(m_cam, CCS_HALT));
            m_status = Ready;
        }

        void Interface::getStatus(StatusType& status)
        {
            DEB_MEMBER_FUNCT();
            switch (m_status)
            {
            case Ready:
                status.set(HwInterface::StatusType::Ready);
                break;
            case Exposure:
                status.set(HwInterface::StatusType::Exposure);
                break;
            case Readout:
                status.set(HwInterface::StatusType::Readout);
                break;
            default:
                status.set(HwInterface::StatusType::Fault);
                break;
            }
            DEB_RETURN() << DEB_VAR1(status);
        }

        int Interface::getNbHwAcquiredFrames()
        {
            DEB_MEMBER_FUNCT();
            return m_acq_frames;
        }

        void Interface::newFrameReady()
        {
            DEB_MEMBER_FUNCT();
            int nb_frame_to_acq;
            m_sync->getNbHwFrames(nb_frame_to_acq);

            StdBufferCbMgr& buffer_mgr = m_buffer_ctrl_obj.getBuffer();
            void* framePt = buffer_mgr.getFrameBufferPtr(m_acq_frames);
            void* buffer_frame;
            if (pl_exp_get_latest_frame(m_cam, &buffer_frame) == PV_FAIL)
            {
                // Something very strange happen
                DEB_ERROR() << "Can't get frame pointer";
                goto error;
            }

            ++m_acq_frames;
            if (nb_frame_to_acq > 0 && m_acq_frames == nb_frame_to_acq)
            {
                m_status = Ready;
                goto end;
            }
            return;			// continue

        error:
            m_status = Fault;
        end:
            pl_exp_stop_cont(m_cam, CCS_HALT);
        }

        void Interface::_freePixelBuffer()
        {
            if (m_pixel_stream)
#ifdef __unix
                free(m_pixel_stream);
#else
                _aligned_free(m_pixel_stream);
#endif
            m_pixel_stream = NULL;
        }

    }
} 