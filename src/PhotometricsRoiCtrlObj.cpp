//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2011
// European Synchrotron Radiation Facility
// BP 220, Grenoble 38043
// FRANCE
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#include "PhotometricsRoiCtrlObj.h"
#include "PhotometricsException.h"

using namespace lima;
using namespace lima::Photometrics;

RoiCtrlObj::RoiCtrlObj(short cam,DetInfoCtrlObj &det) : m_cam(cam),m_det(det)
{
  //Init roi to full frame
  setRoi({0,0,0,0});
}

RoiCtrlObj::~RoiCtrlObj()
{
}

void RoiCtrlObj::setRoi(const Roi& set_roi)
{
  if(set_roi.isActive())
    m_roi = set_roi;
  else				// full frame
    {
      Size max_image_size;
      m_det.getMaxImageSize(max_image_size);
      m_roi = Roi(0,0,max_image_size.getWidth(),max_image_size.getHeight());
    }
}

void RoiCtrlObj::getRoi(Roi &hw_roi)
{
  hw_roi = m_roi;
}

void RoiCtrlObj::checkRoi(const Roi& set_roi, Roi& hw_roi)
{
  // seams to be able to have any roi
  hw_roi = set_roi;
}
