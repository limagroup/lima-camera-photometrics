//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2020
// European Synchrotron Radiation Facility
// CS40220 38043 Grenoble Cedex 9 
// FRANCE
//
// Contact: lima@esrf.fr
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################

#include "master.h"
#include "pvcam.h"

#include "PhotometricsDetInfoCtrlObj.h"
#include "PhotometricsException.h"

using namespace lima;
using namespace lima::Photometrics;

DetInfoCtrlObj::DetInfoCtrlObj(short cam) : m_cam(cam)
{
  DEB_CONSTRUCTOR();

  // Detector width
  rs_bool isAvailable;
  CHECK_PVCAM(pl_get_param(m_cam, PARAM_PIX_PAR_SIZE, ATTR_AVAIL, (void *)&isAvailable));
  if(!isAvailable)
    THROW_HW_ERROR(Error) << "Can't not get detector geometry";
  uns16 width;
  CHECK_PVCAM(pl_get_param(m_cam, PARAM_PIX_PAR_SIZE, ATTR_CURRENT, &width));
  m_max_columns = width;
  // Detector height
  CHECK_PVCAM(pl_get_param(m_cam, PARAM_PIX_SER_SIZE, ATTR_AVAIL, (void *)&isAvailable));
  if(!isAvailable)
    THROW_HW_ERROR(Error) << "Can't not get detector geometry";
  uns16 height;
  CHECK_PVCAM(pl_get_param(m_cam, PARAM_PIX_SER_SIZE, ATTR_CURRENT, &height));
  m_max_rows = height;
}

DetInfoCtrlObj::~DetInfoCtrlObj()
{
}

void DetInfoCtrlObj::getMaxImageSize(Size& max_image_size)
{
  max_image_size = Size(m_max_columns,m_max_rows);
}

void DetInfoCtrlObj::getDetectorImageSize(Size& det_image_size)
{
  getMaxImageSize(det_image_size);
}

void DetInfoCtrlObj::getDefImageType(ImageType& det_image_type)
{
  /* Most Photometrics camera are 16bits Mono
     but some can handle other format... Not managed.
  */
  det_image_type = Bpp16;
}

void DetInfoCtrlObj::getCurrImageType(ImageType& curr_image_type)
{
  curr_image_type = Bpp16;
}

void DetInfoCtrlObj::setCurrImageType(ImageType curr_image_type)
{
  DEB_MEMBER_FUNCT();
  DEB_PARAM() << DEB_VAR1(curr_image_type);

  if(curr_image_type != Bpp16)
    THROW_HW_ERROR(Error) << "Only support 16bits image";
}

void DetInfoCtrlObj::getPixelSize(double& x_size,double &y_size)
{
  x_size = y_size = -1.;	// Don't know (yet) how to read it?
}

void DetInfoCtrlObj::getDetectorType(std::string& det_type)
{
  DEB_MEMBER_FUNCT();
  
  char vendor_name[MAX_VENDOR_NAME_LEN];
  rs_bool isAvailable;
  CHECK_PVCAM(pl_get_param(m_cam, PARAM_VENDOR_NAME, ATTR_AVAIL, (void *)&isAvailable));
  if(!isAvailable)
    det_type = "Photometrics";
  else
    {
      CHECK_PVCAM(pl_get_param(m_cam, PARAM_VENDOR_NAME, ATTR_CURRENT, vendor_name));
      det_type = vendor_name;
    }
}

void DetInfoCtrlObj::getDetectorModel(std::string& det_model)
{
  DEB_MEMBER_FUNCT();
  
  rs_bool isAvailable;
  char product_name[MAX_PRODUCT_NAME_LEN];
  CHECK_PVCAM(pl_get_param(m_cam, PARAM_PRODUCT_NAME, ATTR_AVAIL, (void *)&isAvailable));
  if(!isAvailable)
    {
      det_model = "Unknown";
      return;
    }
  CHECK_PVCAM(pl_get_param(m_cam, PARAM_PRODUCT_NAME, ATTR_CURRENT, product_name));
  det_model = product_name;
  
  char system_name[MAX_SYSTEM_NAME_LEN];
  CHECK_PVCAM(pl_get_param(m_cam, PARAM_SYSTEM_NAME, ATTR_AVAIL, (void *)&isAvailable));
  if(isAvailable)
    {
      char buffer[MAX_SYSTEM_NAME_LEN + 3];
      CHECK_PVCAM(pl_get_param(m_cam, PARAM_SYSTEM_NAME, ATTR_CURRENT, system_name));
      snprintf(buffer,sizeof(buffer)," (%s)",system_name);
      det_model += buffer;
    }
}

void DetInfoCtrlObj::registerMaxImageSizeCallback(HwMaxImageSizeCallback& cb)
{
  m_mis_cb_gen.registerMaxImageSizeCallback(cb);
}

void DetInfoCtrlObj::unregisterMaxImageSizeCallback(HwMaxImageSizeCallback& cb)
{
  m_mis_cb_gen.unregisterMaxImageSizeCallback(cb);
}
