//###########################################################################
// This file is part of LImA, a Library for Image Acquisition
//
// Copyright (C) : 2009-2020
// European Synchrotron Radiation Facility
// CS40220 38043 Grenoble Cedex 9 
// FRANCE
//
// Contact: lima@esrf.fr
//
// This is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 3 of the License, or
// (at your option) any later version.
//
// This software is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, see <http://www.gnu.org/licenses/>.
//###########################################################################
#ifndef PHOTOMETRICSINTERFACE_H
#define PHOTOMETRICSINTERFACE_H
#include <string>
#include <list>

#include "master.h"
#include "pvcam.h"

#include <photometrics_export.h>
#include "lima/HwInterface.h"

namespace lima
{
  namespace Photometrics
  {
    class DetInfoCtrlObj;
    class SyncCtrlObj;
    class BinCtrlObj;
    class RoiCtrlObj;

    struct Process
    {
      unsigned int	id;
      std::string	name;
    };
    void EOFCallbackHandler(FRAME_INFO* pFrameInfo, void* pContext);

    class PHOTOMETRICS_EXPORT Interface : public HwInterface
    {
      friend void EOFCallbackHandler(FRAME_INFO* pFrameInfo, void* pContext);
      DEB_CLASS_NAMESPC(DebModCamera, "PhotometricsInterface", "Photometrics");
    
    public:
      enum Status {Ready, Exposure, Readout, Fault};

      Interface(const std::string& camera_name);
      virtual ~Interface();
      //- From HwInterface
      virtual void	getCapList(CapList&) const;
      virtual void	reset(ResetLevel reset_level);
      virtual void	prepareAcq();
      virtual void	startAcq();
      virtual void	stopAcq();
      virtual void	getStatus(StatusType& status);
      virtual int       getNbHwAcquiredFrames();


      void newFrameReady();
      std::list<Process> getPostProcessing() const;
      
    private:
      void _freePixelBuffer();
      
      short		m_cam;
      DetInfoCtrlObj* 	m_det_info;
      SyncCtrlObj*	m_sync;
      BinCtrlObj*	m_bin;
      RoiCtrlObj*       m_roi;
      SoftBufferCtrlObj m_buffer_ctrl_obj;
      CapList		m_cap_list;

      std::string	m_cam_name;
      int		m_acq_frames;
      Status		m_status;
      void*		m_pixel_stream;	// double buffer
      int		m_exp_frame_bytes;
    };
  
} // namespace Photometrics
} // namespace lima

#endif // PHOTOMETRICSINTERFACE_H
